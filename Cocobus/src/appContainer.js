
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, Button } from 'react-native';
import { createStackNavigator, createAppContainer, createDrawerNavigator } from 'react-navigation'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import Home from './containers/home'
import Login from './containers/login';
import { Icon } from 'native-base'
import Drawer from './components/drawer'
import { color, Color } from './themes/color'
import { Loading, ConfirmBox, Messagebox } from './ui'
import { scale, verticalScale, moderateScale } from './responsive/scaling'


let firstRouter = false


const RootTab = createMaterialBottomTabNavigator({
    tabhome: {
        screen: Home,
        navigationOptions: {
            tabBarColor: 'rgba(142,36,170 ,1)',
            tabBarLabel: `Bán vé`,
            tabBarIcon: ({ focused, horizontal, tintColor }) => (<Icon style={{ color: tintColor, fontSize: moderateScale(22) }} type='Entypo' name='shop' />)
        }
    },
    tabLogin: {
        screen: Login,
        navigationOptions: {
            tabBarColor: 'rgba(194,24,91 ,1)',
            tabBarLabel: 'Quét mã',
            tabBarIcon: ({ focused, horizontal, tintColor }) => (<Icon style={{ color: tintColor, fontSize: moderateScale(22) }} type='MaterialCommunityIcons' name='barcode-scan' />)
        }
    },
    tabLogin1: {
        screen: Login,
        navigationOptions: {
            tabBarColor: 'rgba(211,47,47 ,1)',
            tabBarLabel: 'Tài khoản',
            tabBarIcon: ({ focused, horizontal, tintColor }) => (<Icon style={{ color: tintColor, fontSize: moderateScale(22) }} type='FontAwesome' name='user' />)
        }
    },

}, {
        shifting: true,
        initialRouteName: 'tabhome',
        activeColor: '#fff',
        inactiveColor: '#eeeeee',
        barStyle: { backgroundColor: '#fff', padding: 0, margin: 0 },
        backBehavior: 'none'
    });






const RootStack = createStackNavigator({
    home: {
        screen: Home,
    },
    login: {
        screen: Login
    },
    bottomTab: {
        screen: RootTab
    }
}, {
        initialRouteName: 'bottomTab',
        headerMode: 'none'
    })

const RootDrawer = createDrawerNavigator({
    Home: {
        screen: RootStack,
    },

}, {
        contentComponent: props => <Drawer {...props} />
    });

const RootApp = createAppContainer(RootDrawer)

export default class AppContainer extends Component {

    handleNavigationChange = (prevState, newState, action) => {
        if (newState.index == 0) {
            firstRouter = true
        } else {
            firstRouter = false
        }
        ///handle backAndroid///
    }

    render() {
        return (
            <View style={{ flex: 1 }}>


                {/* Container */}
                <RootApp
                    onNavigationStateChange={this.handleNavigationChange}
                    uriPrefix="/app/thanhvd" />

                {/* UI */}
                <ConfirmBox />
                <Loading />
                <Messagebox />

            </View>)
    }
}
